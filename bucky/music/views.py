from django.shortcuts import render, get_object_or_404
from .models import Album, Song


def index(request):
    albums = Album.objects.all()
#   context = dict([['albums',albums]])   ESTA ES OTRA FORMA DE CREAR DICCIONARIOS
    context = {'albums': albums}
    return render(request, 'music/index.html', context)


def detail(request, album_id):
 #   try:
 #       album = Album.objects.get(id=album_id)
 #   except Album.DoesNotExist:
 #       raise Http404("Album does not exits")
    album = get_object_or_404(Album, id=album_id)
    return render(request, 'music/detail.html', {'album': album})


def favorite(request, album_id):
    album = get_object_or_404(Album, id=album_id)
    try:
        select_song = album.song_set.get(pk=request.POST['song'])
    except(KeyError, Song.DoesNotExist):
        return render(request, 'music/detail.html', {
            'album': album,
            'error_message': "YOU DID NOT SELECT A VALID OPTION",
        })
    else:
        if select_song.favorite:
            select_song.favorite = False
        else:
            select_song.favorite = True
        select_song.save()
        return render(request, 'music/detail.html', {'album': album})

